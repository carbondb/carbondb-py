# carbondb-py

The `carbondb-py` project is a Python implementation of Carbon DB and is meant
to be the reference implementation.

## Get started

The project uses `poetry` to manage dependencies and handle all environment
details with minimal effort. In order to run the code, you will first need
to ensure that poetry is installed on your machine:

```bash
poetry --version
```

If it's not installed, follow the [Installation instructions](https://python-poetry.org/docs/#installation).
The gist of it is repeated here for convenience:

### OS-X or Linux

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

If you are running Ubuntu, you should run a recent version such as 20.04LTS and
you should substitute `python` for `python3` in the command above.

### Windows PowerShell

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```

## Run the Code

Once you have `poetry` installed, you can run the code. Note that you also
need `git` installed but we assume that you viewing those instructions on
`gitlab.com` implies that you have this working already.

- Clone the repository: `git clone https://gitlab.com/carbondb/carbondb-py.git`
- Navigate down to the project folder: `cd carbondb-py`
- Install dependencies: `poetry install`
- Create a virtual shell: `poetry shell` (you can exit the shell just like any other python shell by typing `exit` or `CTRL+D`)
- Run the tests: `poetry run pytest`

If the tests pass, you're all set!

## Generate the Docs

You can generate the documentation by first generating the API docs:

```bash
poetry run sphinx-apidoc -o docs/api src -H 'Carbon DB Packages'
```

And then by generating the full website:

```bash
poetry run sphinx-build docs public
```

The documentation will be available as HTML files in the `public` folder.
