from carbondb import iso8601
import pytest
import datetime

def test_new_duration_days():
    new_duration = iso8601.Duration(iso_duration="P30D")
    assert new_duration.unit == "days"
    assert new_duration.number == "30"

def test_new_duration_time():
    new_duration = iso8601.Duration(iso_duration="PT30M")
    assert new_duration.unit == "minutes"
    assert new_duration.number == "30"

def test_new_duration_invalid():
    with pytest.raises(ValueError, match=r"Invalid duration given"):
        iso8601.Duration(iso_duration="PM0")

def test_add_duration():
    new_duration = iso8601.Duration(iso_duration="PT30M")
    start_date = datetime.datetime.fromisoformat("2001-05-26T01:00:00")
    new_date = new_duration.add_duration(start_date)
    assert new_date == datetime.datetime.fromisoformat("2001-05-26T01:30:00")

def test_subtract_duration():
    new_duration = iso8601.Duration(iso_duration="PT30M")
    start_date = datetime.datetime.fromisoformat("2001-05-26T01:00:00")
    new_date = new_duration.subtract_duration(start_date)
    assert new_date == datetime.datetime.fromisoformat("2001-05-26T00:30:00")
