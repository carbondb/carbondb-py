from fastapi.testclient import TestClient
from fastapi.websockets import WebSocket

from carbondb import config, main

from carbondb.main import app
from . import test_records

import datetime
import json

client = TestClient(app)

def get_settings_override():
    return config.Settings(admin_email="testing_admin@example.com", app_name="Awesome API")

app.dependency_overrides[main.get_settings] = get_settings_override

def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World!"}

def test_post_read_series():
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    assert new_series["period"] == "PT1H"
    assert new_series["unit"] == "kwh"
    assert new_series["aggregation"] == "sum"
    new_series_id = new_series["id"]
    read_response = client.get("/series/?limit=1000")
    assert read_response.status_code == 200
    id_response = client.get(f"/series/{new_series_id}")
    series_added = id_response.json()
    assert series_added == new_series

def test_post_series_invalid_period():
    post_response = client.post(
        "/series/",
        json={"period": "1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 422

def test_edit_series_invalid_period():
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    assert new_series["period"] == "PT1H"
    assert new_series["unit"] == "kwh"
    new_id = new_series["id"]
    update_id_response = client.put(
        f"/series/{new_id}",
        json={"period": "30M", "unit": "gbp", "aggregation": "sum"},
    )
    assert update_id_response.status_code == 422

def test_post_read_id_series():
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    assert new_series["period"] == "PT1H"
    assert new_series["unit"] == "kwh"
    new_id = new_series["id"]
    read_id_response = client.get(f"/series/{new_id}")
    assert read_id_response.status_code == 200
    series_get_id = read_id_response.json()
    assert series_get_id == new_series

def test_post_edit_id_series():
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    assert new_series["period"] == "PT1H"
    assert new_series["unit"] == "kwh"
    new_id = new_series["id"]
    update_id_response = client.put(
        f"/series/{new_id}",
        json={"period": "PT30M", "unit": "gbp", "aggregation": "sum"},
    )
    assert update_id_response.status_code == 200
    series_edit_id = update_id_response.json()
    assert series_edit_id["period"] == "PT30M"
    assert series_edit_id["unit"] == "gbp"

def test_post_delete_id_series():
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    assert new_series["period"] == "PT1H"
    assert new_series["unit"] == "kwh"
    new_id = new_series["id"]
    delete_id_response = client.delete(f"/series/{new_id}")
    assert delete_id_response.status_code == 200
    read_id_response = client.get(f"/series/{new_id}")
    assert read_id_response.status_code == 404
    assert json.loads(read_id_response.text) == { "message": "No timeseries found" }
    update_id_response = client.put(
        f"/series/{new_id}",
        json={"period": "PT30M", "unit": "gbp", "aggregation": "sum"},
    )
    assert update_id_response.status_code == 404
    assert json.loads(update_id_response.text) == { "message": "No timeseries found" }

def test_post_get_record():
    # need to get an id for a timeseries
    post_response = client.post(
        "/series/",
        json={"period": "PT1H", "unit": "kwh", "aggregation": "sum"},
    )
    assert post_response.status_code == 200
    new_series = post_response.json()
    new_series_id = new_series["id"]
    post_response = client.post(
        f"/series/{new_series_id}/records",
        json={
            "r": [
                {"t": "2001-05-26T01:00Z", "v": 1.1},
                {"t": "2001-05-26T02:00Z", "v": 1.2},
                {"t": "2001-05-26T03:00Z", "v": 1.3},
                {"t": "2001-05-26T04:00Z", "v": 1.4},
                {"t": "2001-05-26T05:00Z", "v": 1.5}
                ]
            },
    )
    assert post_response.status_code == 204
    read_records_response = client.get(f"/series/{new_series_id}/records")
    assert read_records_response.status_code == 200
    records_get = read_records_response.json()
    assert records_get["r"][0]["v"] == [1.1]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 5

def test_get_record_no_id():
    read_records_response = client.get(f"/series/abcd/records")
    assert read_records_response.status_code == 404
    assert json.loads(read_records_response.text) == { "message": "No timeseries found" }

def test_post_record_no_id():
    post_response = client.post(
        f"/series/abcd/records",
        json={
            "r": [
                {"t": "2001-05-26T01:00Z", "v": 1.1},
                {"t": "2001-05-26T02:00Z", "v": 1.2},
                {"t": "2001-05-26T03:00Z", "v": 1.3},
                {"t": "2001-05-26T04:00Z", "v": 1.4}
                ]
            },
    )
    assert post_response.status_code == 404

def test_get_record_slice_no_id():
    read_records_response = client.get(f"/series/abcd/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/-")
    assert read_records_response.status_code == 404
    assert json.loads(read_records_response.text) == { "message": "No timeseries found" }


def test_get_record_slice():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/-")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [1.1]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 4
    # test that records are returned with None values when outside of limits
    read_records_slice_response = client.get(f"/series/{series_id}/records/2002-05-26T01%3A00%3A00/2002-05-26T03%3A00%3A00/-")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert len(records_get["r"]) == 4
    assert records_get["r"][0]["v"] == [None]
    # check timeseries id returned
    assert records_get["n"] == [f"{series_id}|sum"]

def test_get_record_slice_interval():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/PT1H")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [2.2]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 2
    # test that records return for 1 day interval
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T00%3A00%3A00/2001-05-28T00%3A00%3A00/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 2

def test_get_record_slice_interval_invalid_date():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05/2001-05-26T03%3A00%3A00/PT1H")
    assert read_records_slice_response.status_code == 422
    assert json.loads(read_records_slice_response.text) == { "message": "Invalid start date entered" }

def test_get_record_slice_interval_no_start():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/-/2001-05-26T03%3A00%3A00/PT1H")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [2.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 3
    # test that records return for 1 day interval
    read_records_slice_response = client.get(f"/series/{series_id}/records/-/2001-05-28T00%3A00%3A00/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 2

def test_get_record_slice_interval_no_end():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/-/PT1H")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [2.2]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 71
    # test that records return for 1 day interval
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-27T00%3A00%3A00/-/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-27T00:00:00+00:00"
    assert len(records_get["r"]) == 2

def test_get_record_slice_interval_no_start_and_end():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/-/-/PT1H")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [2.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 72
    # test that records return for 1 day interval
    read_records_slice_response = client.get(f"/series/{series_id}/records/-/-/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 3

def test_get_record_slice_interval_mma():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/PT1H?aggregation=mma")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [1.1,1.1,1.1]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 2
    # check timeseries id returned
    assert records_get["n"] == [f"{series_id}|min", f"{series_id}|max", f"{series_id}|avg"]



def test_get_record_slice_interval_with_none():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days_none
            }
    )
    assert post_response.status_code == 204
    # test that records within limit are returned - for 1 hour intervals with None
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T00%3A00%3A00/2001-05-26T02%3A00%3A00/PT1H")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [1.1]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert records_get["r"][1]["v"] == [None]
    assert records_get["r"][1]["t"] == "2001-05-26T01:00:00+00:00"
    assert len(records_get["r"]) == 2
    # test that records return for 1 day interval - with multiple None values excluded
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T00%3A00%3A00/2001-05-28T00%3A00%3A00/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [65.9]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 2
    # test that slice wider than timeseries holds None values for those dates
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T00%3A00%3A00/2001-05-30T00%3A00%3A00/P1D")
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][-1]["v"] == [None]
    assert records_get["r"][-1]["t"] == "2001-05-29T00:00:00+00:00"
    assert len(records_get["r"]) == 4

def test_get_record_slice_invalid_interval():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days_none
            }
    )
    assert post_response.status_code == 204
    # test that records with invalid interval return 422
    read_records_slice_response = client.get(f"/series/{series_id}/records/2001-05-26T00%3A00%3A00/2001-05-26T02%3A00%3A00/P1H")
    assert read_records_slice_response.status_code == 422
    assert json.loads(read_records_slice_response.text) == { "message": "Invalid interval entered" }

def test_get_record_slice_timezone():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    # test that records within limit are returned
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/PT1H?tz=US/Eastern",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [3]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00-04:00"
    assert len(records_get["r"]) == 2

def test_get_record_slice_timezone_london_summer():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop(0)
    series_id = test_series["id"]
    # add days worth of records
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    # test that records within limit are returned
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2001-05-26T01%3A00%3A00/2001-05-26T03%3A00%3A00/PT1H?tz=Europe/London",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [2.2]
    assert records_get["r"][0]["t"] == "2001-05-26T01:00:00+01:00"
    assert len(records_get["r"]) == 2

def test_get_record_slice_timezone_london_winter():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2001-01-26T01%3A00%3A00/2001-01-26T03%3A00%3A00/-?tz=Europe/London",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2001-01-26T01:00:00+00:00"
    assert len(records_get["r"]) == 4

def test_get_record_slice_timezone_bst():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2020-01-01T00%3A00%3A00/2020-01-02T00%3A00%3A00/PT1H?tz=Europe/London",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2020-01-01T00:00:00+00:00"
    assert len(records_get["r"]) == 24
    # test that records within limit are returned for day clocks go forward
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2020-03-29T00%3A00%3A00/2020-03-30T00%3A00%3A00/PT1H?tz=Europe/London",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2020-03-29T00:00:00+00:00"
    assert len(records_get["r"]) == 23
    # test that records within limit are returned for day clocks go backward
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2020-10-25T00%3A00%3A00/2020-10-26T00%3A00%3A00/PT1H?tz=Europe/London",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2020-10-25T00:00:00+01:00"
    assert len(records_get["r"]) == 25

def test_get_record_slice_interval_end():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/2021-06-01/P1W/P1D",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2021-06-01T00:00:00+00:00"
    assert len(records_get["r"]) == 7
    assert records_get["r"][-1]["t"] == "2021-06-07T00:00:00+00:00"

def test_get_record_slice_interval_start():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/P10D/2021-06-20/P1D",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [None]
    assert records_get["r"][0]["t"] == "2021-06-10T00:00:00+00:00"
    assert len(records_get["r"]) == 10
    assert records_get["r"][-1]["t"] == "2021-06-19T00:00:00+00:00"

def test_get_record_slice_interval_both():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/P10D/P1D/P1D",
        )
    assert read_records_slice_response.status_code == 422
    assert json.loads(read_records_slice_response.text) == { "message": "Both start and end date can not be a duration" }

def test_get_record_slice_interval_end_no_start():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/-/P3D/P1D",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 3
    assert records_get["r"][-1]["t"] == "2001-05-28T00:00:00+00:00"
    assert records_get["r"][-1]["v"] == [69.2]

def test_get_record_slice_interval_start_no_end():
    # need to get an id for a timeseries
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    post_response = client.post(
        f"/series/{series_id}/records",
        json={
            "r": test_records.test_records_days
            }
    )
    # test that records within limit are returned for regular 24 hour day
    read_records_slice_response = client.get(
        f"/series/{series_id}/records/P3D/-/P1D",
        )
    assert read_records_slice_response.status_code == 200
    records_get = read_records_slice_response.json()
    assert records_get["r"][0]["v"] == [69.2]
    assert records_get["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
    assert len(records_get["r"]) == 3
    assert records_get["r"][-1]["t"] == "2001-05-28T00:00:00+00:00"
    assert records_get["r"][-1]["v"] == [69.2]

def test_websocket_slice():
    client = TestClient(app)
    # get id
    read_series_response = client.get("/series/")
    series_array = read_series_response.json()
    test_series = series_array.pop()
    series_id = test_series["id"]
    # create json to send
    params = {
        "id": series_id,
    }
    with client.websocket_connect("/series/stream") as websocket:
        # send json
        websocket.send_json(params)
        data = websocket.receive_json()
        assert data["r"][0]["v"] == [1.1]
        assert data["r"][0]["t"] == "2001-05-26T00:00:00+00:00"
        assert len(data["r"]) == 144
        assert data["r"][-1]["t"] == "2001-05-28T23:30:00+00:00"
        assert data["r"][-1]["v"] == [1.5]

    #test websocket
def test_websocket():
    client = TestClient(app)
    with client.websocket_connect("/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"msg": "Hello WebSocket"}

# Fast api tutorial example test
def test_app():

    response = client.get("/info")
    data = response.json()
    assert data == {
        "app_name": "Awesome API",
        "admin_email": "testing_admin@example.com",
        "items_per_user": 50,
    }
