from carbondb import downsample
from datetime import datetime
from carbondb.iso8601 import Duration
from carbondb.schemas import TimeseriesRecordBase, TimeseriesSliceRecordBase
import pytest

def test_generate_record_list():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.generate_record_list(records)
    new_list = [r for r in generator]
    assert new_list == records

def test_generate_record_list_next():
    records = [
        {"t": datetime.fromisoformat("2001-05-26T00:00"), "v": 1.1},
        {"t": datetime.fromisoformat("2001-05-26T00:30"), "v": 1.1},
        {"t": datetime.fromisoformat("2001-05-26T01:00"), "v": 1.1},
        {"t": datetime.fromisoformat("2001-05-26T01:30"), "v": 1.1},
        {"t": datetime.fromisoformat("2001-05-26T02:00"), "v": 1.2},
        {"t": datetime.fromisoformat("2001-05-26T02:30"), "v": 1.2},
    ]
    generator = downsample.generate_record_list(records)
    first_item = next(generator)
    assert first_item == records[0]

def test_group_to_buckets():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.group_to_buckets(iter(records), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T00:00"), "UTC"), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T02:00"), "UTC"), Duration("PT1H"))
    new_list = [r for r in generator]
    assert new_list == [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[1.1, 1.1]),
    ]

def test_group_to_buckets_long():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.group_to_buckets(iter(records), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T00:00"), "UTC"), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T04:00"), "UTC"), Duration("PT1H"))
    new_list = [r for r in generator]
    assert new_list == [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T02:00+00:00"), v=[1.2, 1.2]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T03:00+00:00"), v=[]),
    ]

def test_group_to_buckets_none():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.group_to_buckets(iter(records), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2002-05-26T00:00"), "UTC"), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2002-05-26T04:00"), "UTC"), Duration("PT1H"))
    new_list = [r for r in generator]
    assert new_list == [
        downsample.Bucket(t=datetime.fromisoformat("2002-05-26T00:00+00:00"), v=[]),
        downsample.Bucket(t=datetime.fromisoformat("2002-05-26T01:00+00:00"), v=[]),
        downsample.Bucket(t=datetime.fromisoformat("2002-05-26T02:00+00:00"), v=[]),
        downsample.Bucket(t=datetime.fromisoformat("2002-05-26T03:00+00:00"), v=[]),
    ]

def test_group_to_buckets_long_start():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-25T23:00"), v=1.0),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.group_to_buckets(iter(records), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T00:00"), "UTC"), downsample.convert_local_datetime_to_utc(datetime.fromisoformat("2001-05-26T04:00"), "UTC"), Duration("PT1H"))
    new_list = [r for r in generator]
    assert new_list == [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T02:00+00:00"), v=[1.2, 1.2]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T03:00+00:00"), v=[]),
    ]

def test_aggregate_buckets():
    records = [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.1]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[1.1, 1.1]),
    ]
    generator = downsample.aggregate_buckets(records, "sum", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[2.2]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.2]),
    ]

def test_aggregate_buckets_mma():
    records = [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.2]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.0, 2.2]),
    ]
    generator = downsample.aggregate_buckets(records, "mma", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[1.1, 1.2, 1.15]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.0, 2.2, 2.1])
    ]

def test_aggregate_buckets_none():
    records = [
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[]),
        downsample.Bucket(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[1.1, 1.1]),
    ]
    generator = downsample.aggregate_buckets(records, "sum", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[None]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.2]),
    ]

def test_convert_local_datetime_to_utc():
    local_date_api = datetime.fromisoformat("2001-05-26T00:00")
    tz = "Europe/London"
    utc_date = downsample.convert_local_datetime_to_utc(local_date_api, tz)
    assert utc_date == datetime.fromisoformat("2001-05-25T23:00+00:00")

def localize_naive_utc_date():
    naive_utc_date_api = datetime.fromisoformat("2001-05-26T00:00")
    utc_date = downsample.localize_naive_utc_date(local_date_api)
    assert utc_date == datetime.fromisoformat("2001-05-26T00:00+00:00")

def test_convert_utc_datetime_to_local():
    utc_datetime = datetime.fromisoformat("2001-05-25T23:00+00:00")
    tz = "Europe/London"
    local_date = downsample.convert_utc_datetime_to_local(utc_datetime, tz)
    assert local_date == datetime.fromisoformat("2001-05-26T00:00+01:00")

def test_downsample():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.downsample(records, datetime.fromisoformat("2001-05-26T00:00+00:00"), datetime.fromisoformat("2001-05-26T02:00+00:00"), Duration("PT1H"),"PT30M", "sum", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[2.2]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.2]),
    ]

def test_downsample_no_start():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.downsample(records, None, datetime.fromisoformat("2001-05-26T02:00+00:00"), Duration("PT1H"),"PT30M", "sum", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[2.2]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.2]),
    ]

def test_downsample_no_end():
    records = [
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T00:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:00"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T01:30"), v=1.1),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:00"), v=1.2),
        TimeseriesRecordBase(t=datetime.fromisoformat("2001-05-26T02:30"), v=1.2),
    ]
    generator = downsample.downsample(records, datetime.fromisoformat("2001-05-26T00:00+00:00"), None, Duration("PT1H"),"PT30M", "sum", "UTC")
    new_list = [r for r in generator]
    assert new_list == [
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T00:00+00:00"), v=[2.2]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T01:00+00:00"), v=[2.2]),
        TimeseriesSliceRecordBase(t=datetime.fromisoformat("2001-05-26T02:00+00:00"), v=[2.4]),
    ]
