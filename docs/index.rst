.. carbondb documentation master file, created by
   sphinx-quickstart on Tue May  4 15:16:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Carbon DB Python Implementation
===============================

What is it?
-----------

This is the home of the Python reference implementation of `Carbon DB`_, a
timeseries database dedicated to storing energy and carbon data. The Python
implementation is designed to provide a simple implementation that runs on
top of a SQL database and can be deployed on premise or in the cloud.

How to get started?
-------------------

To get started, go to the `GitLab repository`_ and follow the instructions
to run the code. If it doesn't work as expected, please tell us by opening
an issue in GitLab.

How to contribute?
------------------

You can contribute at various levels in the project. Depending on the type
of contribution, you may need to have experience with Python. Here's the
type of things you can get involved in:

* Improve the documentation by finding typos, errors or adding brand new
  sections
* Contribute to some of the implementation

The contribution process is as follows:

1. A Contributor opens an issue in GitLab describing the problem or the feature
   needed
2. A Maintainer reviews the issue and assigns it to a Contributor
3. A Contributor fixes the problem or implements the feature
4. A Maintainer merges the change into the main documentation or code branch

All contributions are subject to our Governance process and Code of Conduct,
which you will find below. Please make sure you read them before contributing.

Supporting information
~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   governance
   code_of_conduct
   api/modules


.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. _Carbon DB: https://carbondb.tech/
.. _GitLab repository: https://gitlab.com/carbondb/carbondb-py
