Project Governance
==================

This project is in its infancy and so is its governance. That said, here's a
start for how it all works.

Roles
-----

Contributors can play the following roles in the project:

Lead Maintainer
^^^^^^^^^^^^^^^

As this project is currently led by `imbytech`_, the project lead and lead
maintainer is imby's CTO, Bruno Girin. In addition to the responsibilities
of the Core Maintainers, the Lead Maintainer is responsible for:

* The overall solution design
* Prioritising issues
* Reviewing merge requests from Core Maintainers and Contributors

Core Maintainers
^^^^^^^^^^^^^^^^

The Core Maintainers are current the `imbytech`_ team and their responsibilities
include:

* Implementing core functionality
* Reviewing merge requests from Contributors

Contributors
^^^^^^^^^^^^

Contributors are all people who contribute to the project, whether it be through
issues or merge requests. Contributions can include documentation, code, or
anything that improves the project. No particular qualification is required to
become a contributor.

Governance Changes
------------------

Any changes to the list of roles or any other governance aspect should be done
by opening a project issue in GitLab and tagging it with the `Governance` label.
Explain the aspect of governance you would like to change and why.

This project is currently run by a private company, imby and the Lead Maintainer
is a single individual. This is appropriate for the moment but will need to change
if this project is used more widely. We welcome any suggestion on how to
implement proper governance to transition to a more open model in the future.

Policies and Procedures
-----------------------

We don't yet have many policies and procedures. The key one is our
:doc:`Code of Conduct <code_of_conduct>`.

.. _imbytech: https://www.imbytech.com/
