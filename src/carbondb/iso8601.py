import datetime
# from datetime import date
import re
from dateutil.relativedelta import relativedelta

class Duration:
    """A class used to represent a duration in ISO 8601 format

    ...

    Attributes
    ----------
    iso_duration: str
        a str representing the duration in ISO 8601 format
    unit: str
        the unit of time the duration refers to e.g. days, weeks, hours
    number: int
        the amount of units the duration refers to

    Methods
    -------
    add_duration(self, startdate)
        Add the duration to a datetime instance.
    subtract_duration(self, startdate)
        Subtract the duration from a datetime instance.
    """
    def __init__(self, iso_duration: str):
        """ Parses the iso_duration string and validates that it is a Duration
        in the ISO 8601 format.
        Parameters
        ----------
        iso_duration: str
            The duration in ISO 8601 format

        Raises
        ------
        ValueError
            If the iso_duration is not a valid duration of ISO 8601 format.
        """

        self.iso_duration = iso_duration.upper()
        # is the interval an iso duration - of single unit?
        correct_iso_days = re.fullmatch(r"P(\d+)([Y,M,W,D])", iso_duration)
        correct_iso_time = re.fullmatch(r"PT(\d+)([H,M,S])", iso_duration)
        is_correct = bool(correct_iso_days) or bool(correct_iso_time)
        print(f"is correct {is_correct}")
        # if not raise an exception
        if is_correct == False:
            raise ValueError("Invalid duration given")
        # get time unit (single only allowed)
        days_periods = {
            'Y': "years",
            'M': "months",
            'W': "weeks",
            'D': "days",
        }
        time_periods = {
            'H': "hours",
            'M': "minutes",
            'S': "seconds"
        }
        initial_unit = iso_duration[-1]
        self.unit = days_periods[initial_unit] if bool(correct_iso_days) else time_periods[initial_unit]
        print(f"unit {self.unit}")
        # get number of units
        self.number = correct_iso_days.group(1) if bool(correct_iso_days) else correct_iso_time.group(1)


    def add_duration(self, startdate):
        """Adds the duration to a datetime instance.

        Parameters
        ----------
        startdate: datetime
            The datetime that is the starting point.

        Raises
        ------
        ValueError
            If the starttime given is not a valid datetime.
        """
        if type(startdate) is not datetime.datetime:
            raise ValueError("Invalid startdate given")
        kw = {self.unit: int(self.number)}
        new_datetime = startdate + relativedelta(**kw)
        return new_datetime

    def subtract_duration(self, startdate):
        """Subtracts the duration from a datetime instance.

        Parameters
        ----------
        startdate: datetime
            The datetime that is the starting point.

        Raises
        ------
        ValueError
            If the starttime given is not a valid datetime.
        """
        if type(startdate) is not datetime.datetime:
            raise ValueError("Invalid startdate given")
        kw = {self.unit: -int(self.number)}
        new_datetime = startdate + relativedelta(**kw)
        return new_datetime
