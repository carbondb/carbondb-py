""" Scehmas

This module contains the schemas
"""

from typing import List, Optional, Dict

from pydantic import BaseModel
from datetime import datetime

class TimeseriesBase(BaseModel):
    """ Schema represents the base timeseries information.
    period: str
        An ISO 8601 format duration
    unit: str
        One of: kWh, GBP
    aggregation: str
        The default method for aggregating the timeseries' records
     """
    period: str
    unit: str
    aggregation: str

class Timeseries(TimeseriesBase):
    """ Schema represents the total timeseries information
    id: str
        id for the timeseries
    Plus the information in TimserseriesBase
    """
    id: str

    class Config:
        orm_mode = True

class TimeseriesCreate(TimeseriesBase):
    """ Schema represents timeseries information for creation of model
    Uses the TimseriesBase model
     """
    pass

class TimeseriesRecordBase(BaseModel):
    """ Schema represents the base record information.
    t: datetime
        The timestamp (ISO 8601 utc format) for the record value
    v: float (optional)
        The sampling value at the timestamp t
     """
    t: datetime
    v: Optional[float]

class TimeseriesRecord(TimeseriesRecordBase):
    """ Schema represents the total record information
    timeseries_id: str
        id for the timeseries the record is associated with
    Plus the information in TimserseriesRecordBase
    """
    timeseries_id: str
    class Config:
        orm_mode = True

class TimeseriesSliceRecordBase(BaseModel):
    """ Schema represents the base information for a record in a slice.
    t: datetime
        The timestamp (ISO 8601 utc format) for the record value
    v: List of float (optional)
        The sampling values at the timestamp t
     """
    t: datetime
    v: List[Optional[float]]

# The TimeseriesRecordSet will need to be able to support more complex structures.
# It will need to be disconnected from the ORM, the method of explicit transformation needs revisiting.

class TimeseriesRecordSet(BaseModel):
    """ Schema represents the base information for a set of records.
    r: List of TimeseriesRecordBase
        Records associated with one timeseries
     """
    r: List[TimeseriesRecordBase] = []

class TimeserieSliceRecordSet(BaseModel):
    """ Schema represents the total slice information.
    n: List[str]
        set of id and aggregation methods for the timeseries the records are associated with
    r: List[TimeseriesSliceRecordBase]
        set of TimeseriesSliceRecords
    """
    n: List[str] = []
    r: List[TimeseriesSliceRecordBase] = []

# Fastapi examples

class ItemBase(BaseModel):
    title: str
    description: Optional[str] = None


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    items: List[Item] = []

    class Config:
        orm_mode = True
