"""CRUD

This module contains functions to manipulate the database.

This file can be imported as a module and contains the following functions:
    A group of functions that manipulate the timeseries:
        * get_series - returns all timeseries in the db
        * create_series - adds a timeseries to the db
        * get_series_by_id - returns the timeseries associated with the id
        * update_series_by_id - updates the timeseries associated with the id
        * delete_series_by_id - deletes the timeseries associated with the id
    A group of functions that manipulate the timeseries records:
        * get_series_records - returns the records for the timeseries associated
        with the id given
        * create_series_records - adds (or updates) records for the timeseries associated
        with the id given to the db
        * get_series_records_slice - returns records for the timeseries associated
        with the id for the time period between the dates given and the specified
        sampling rate
"""

from sqlalchemy.orm import Session

from . import models, schemas, exceptions, iso8601, downsample
import uuid
import datetime
from datetime import date

def get_series(db: Session, skip: int = 0, limit: int = 100):
    """returns all timeseries in the db

    Parameters
    ----------
    db : Session
        The database

    Returns
    -------
    Iterable[Timeseries]
        List of all Timeseries in the db
    """
    return db.query(models.Timeseries).offset(skip).limit(limit).all()

def create_series(db: Session, series: schemas.TimeseriesCreate):
    """adds a timeseries to the db

    Parameters
    ----------
    db : Session
        The database
    series: TimeseriesCreate
        The timeseries to add

    Returns
    -------
    Timeseries
        The timeseries that has been added
    """
    uid = str(uuid.uuid4())
    db_series = models.Timeseries(period=series.period, unit=series.unit, id=uid, aggregation=series.aggregation)
    db.add(db_series)
    db.commit()
    db.refresh(db_series)
    return db_series

def get_series_by_id(db: Session, id: str):
    """returns the timeseries associated with the id

    Parameters
    ----------
    db : Session
        The database
    id: str
        The id of the timeseries to return

    Returns
    -------
    Timeseries
        The timeseries associated with the id

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    series_found = db.query(models.Timeseries).filter(models.Timeseries.id == id).first()
    if series_found is None:
        raise exceptions.NotFoundException(message="No timeseries found")
    return series_found

def update_series_by_id(db: Session, id: str, series: schemas.TimeseriesCreate):
    """updates the timeseries associated with the id

    Parameters
    ----------
    db : Session
        The database
    id: str
        The id of the timeseries to update
    series: TimeseriesCreate
        The updated timeseries

    Returns
    -------
    Timeseries
        The timeseries that has been updated

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    series_to_update = db.query(models.Timeseries).filter(models.Timeseries.id == id).first()
    if series_to_update is None:
        raise exceptions.NotFoundException(message="No timeseries found")

    series_to_update.unit = series.unit
    series_to_update.period = series.period
    series_to_update.aggregation = series.aggregation
    db.commit()
    db.refresh(series_to_update)
    return series_to_update

def delete_series_by_id(db: Session, id: str):
    """deletes the timeseries associated with the id

    Parameters
    ----------
    db : Session
        The database
    id: str
        The id of the timeseries to delete

    Returns
    -------
    Dict
        A confirmation message

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    series_to_delete = db.query(models.Timeseries).filter(models.Timeseries.id == id).first()
    if series_to_delete is None:
        raise exceptions.NotFoundException(message="No timeseries found")
    db.delete(series_to_delete)
    db.commit()
    return { "message": "timeseries deleted" }

def get_series_records(db: Session, timeseries_id: str):
    """returns the records for the timeseries associated with the id given

    Parameters
    ----------
    db : Session
        The database
    timeseries_id: str
        The id of the timeseries for the records to return

    Returns
    -------
    TimeseriesRecordSet
        An object containing n - the timeseries_id and r - the records
        associated for that timeseries.

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    timeseries = db.query(models.Timeseries).filter(models.Timeseries.id == timeseries_id).first()
    if timeseries is None:
        raise exceptions.NotFoundException(message="No timeseries found")
    series_records_array = db.query(models.TimeseriesRecord).filter(models.TimeseriesRecord.timeseries_id == timeseries_id).order_by(models.TimeseriesRecord.t).all()
    timeseries_id_array = [timeseries_id]
    record_object = {
        "n": timeseries_id_array,
        "r": series_records_array
    }
    return record_object

def create_series_records(db: Session, timeseries_id: str, record_set: dict):
    """adds records for the timeseries associated with the id given to the
    db - if a record already exists for the timestamp the record is updated with
    the given value.

    Parameters
    ----------
    db : Session
        The database
    timeseries_id: str
        The id of the timeseries for the records to return
    record_set: Dict
        A dict containing a t - timestamp and r - value

    Returns
    -------
    Nothing

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    timeseries = db.query(models.Timeseries).filter(models.Timeseries.id == timeseries_id).first()
    if timeseries is None:
        raise exceptions.NotFoundException(message="No timeseries found")

    new_records = record_set.r
    series_records_list = db.query(models.TimeseriesRecord).filter(models.TimeseriesRecord.timeseries_id == timeseries_id).all()
    def add_update_records(record):
        # records for the timeseries that have a timestamp not already there should be added
        if not any(datetime.datetime.combine(rec.t.date(), rec.t.time(), datetime.timezone(datetime.timedelta())) == record.t for rec in series_records_list):
            new_record = models.TimeseriesRecord(timeseries_id=timeseries_id, t=record.t, v=record.v)
            db.add(new_record)
            db.commit()
            db.refresh(new_record)
        # records that have a timestamp already there should be updated
        else:
            record_to_update = db.query(models.TimeseriesRecord).filter(models.TimeseriesRecord.timeseries_id == timeseries_id).filter(models.TimeseriesRecord.t == record.t).first()
            record_to_update.v = record.v
            db.commit()
            db.refresh(record_to_update)

    def add_records(record):
        new_record = models.TimeseriesRecord(timeseries_id=timeseries_id, t=record.t, v=record.v)
        db.add(new_record)
        db.commit()
        db.refresh(new_record)


    for record in new_records:
        if series_records_list is not None:
            add_update_records(record)
        else:
            add_records(record)


def get_series_records_slice(db: Session, timeseries_id: str, start: datetime, end: datetime):
    """returns records for the timeseries associated with the id for the time
    period between the dates given

    Parameters
    ----------
    db : Session
        The database
    timeseries_id: str
        The id of the timeseries for the records to return
    start: DateTime
        A datetime (utc) that is the start of the time period of the
        required records.
    end: DateTime
        A ISO 8601 format date (utc) that is the end of the time period of the
        required records.
    Returns
    -------
    List of TimeSeriesRecord
        The TimeseriesRecord associated with the Timeseries for the period
        requested

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    filters = [models.TimeseriesRecord.timeseries_id == timeseries_id]

    if type(start) == datetime.datetime:
        filters.append(models.TimeseriesRecord.t >= start)

    if type(end) == datetime.datetime:
        filters.append(models.TimeseriesRecord.t < end)

    series_records_list = db.query(models.TimeseriesRecord).filter(
        *filters
        ).order_by(models.TimeseriesRecord.t).all()
    return series_records_list

def get_last_series_record(db: Session, timeseries_id: str):
    """returns the latest record for the timeseries associated with the id

    Parameters
    ----------
    db : Session
        The database
    timeseries_id: str
        The id of the timeseries for the records to return
    Returns
    -------
    The latest TimeSeriesRecord
        The last TimeseriesRecord associated with the Timeseries

    Raises
    ------
    NotFoundException
        If there is no timeseries associated with the id given.
    """
    last_series_record = db.query(models.TimeseriesRecord).filter(
        models.TimeseriesRecord.timeseries_id == timeseries_id
        ).order_by(models.TimeseriesRecord.t.desc()).first()
    return last_series_record

# Fastapi example code

def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = models.User(email=user.email, hashed_password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_items(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Item).offset(skip).limit(limit).all()


def create_user_item(db: Session, item: schemas.ItemCreate, user_id: int):
    db_item = models.Item(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
