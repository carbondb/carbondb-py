
class NotFoundException(Exception):
    """A exception class used when an db record can not be found for the input

    ...

    Attributes
    ----------
    message: str
        a message to explain the db record that cannot be found.
    """
    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            a message to explain the db record that cannot be found.
        """
        # Call Exception.__init__(message)
        # to use the same Message header as the parent class
        super().__init__(message)
        self.message = message

class InvalidException(Exception):
    """A exception class used when a parameter given is invalid

    ...

    Attributes
    ----------
    message: str
        a message to explain the parameter is invalid.
    """
    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            a message to explain the parameter is invalid.
        """
        # Call Exception.__init__(message)
        # to use the same Message header as the parent class
        super().__init__(message)
        self.message = message
