"""Main

This module contains the Fastapi calls for carbondb.

This file can be imported as a module and contains the following functions:
    Internal functions:
        * get_db - returns a Session for the carbondb database
        * get_settings - returns the settings for the API
    Error handling functions:
        * not_found_exception_handler - handles the NotFoundException and returns a
        404 response
        * invalid_exception_handler - handles the InvalidException and returns a
        422 response
    Functions implementing the REST API:
        * read_series - returns the timeseries stored in the database
        * create_series - adds a tmeseries to the database
        * read_series_by_id - returns the timeseries associated with the id from the
        database
        * edit_series_by_id - updates the database with the new timeseries
        associated with the id
        * remove_series_by_id - removes the timeseries associated with the id from
        the database
        * read_series_records - returns the records for a given timeseries
        * create_series_records - adds records for a given timeseries to the database
        * read_series_records_slice - returns records for a given timeseries between
        two dates
"""

import uvicorn
from fastapi import FastAPI, Depends, HTTPException, status, Response, Request, WebSocket
from fastapi.responses import JSONResponse
from carbondb.hello_world import hello

from sqlalchemy.orm import Session
from typing import List, Optional

from . import crud, models, schemas, config, exceptions, iso8601, downsample
import datetime
import pytz
import json

from functools import lru_cache

app = FastAPI()

# Dependencies
@lru_cache()
def get_settings():
    return config.Settings()

def get_db(settings = Depends(get_settings)):
    session = models.getOrmSession(settings)
    try:
        yield session
    finally:
        session.close()

@app.exception_handler(exceptions.NotFoundException)
async def not_found_exception_handler(request: Request, exc: exceptions.NotFoundException):
    return JSONResponse(
        status_code=404,
        content={"message": exc.message},
    )
@app.exception_handler(exceptions.InvalidException)
async def invalid_exception_handler(request: Request, exc: exceptions.InvalidException):
    return JSONResponse(
        status_code=422,
        content={"message": exc.message},
    )

@app.get("/series/", response_model=List[schemas.Timeseries])
def read_series(skip: int = 0, limit: int = 500, db: Session = Depends(get_db)):
    """list timeseries

    Parameters
    ----------
    skip: int, default 0
        Skips the first n items from the response body
    limit: int, default 500
        Limits the number of records returned to the defined limit (or default 500)

    Returns
    -------
    List[Timeseries]
    """
    timeseries = crud.get_series(db, skip=skip, limit=limit)
    return timeseries

@app.post("/series/", response_model=schemas.Timeseries)
def create_series(series: schemas.TimeseriesCreate, db: Session = Depends(get_db)):
    """add a timeseries

    Parameters
    ----------
    series: TimeseriesCreate
        The series to add to the database

    Returns
    -------
    Timeseries: that has been created
    """
    try:
        new_duration = iso8601.Duration(series.period)
    # catch value error from duration raise exception
    except ValueError:
        raise exceptions.InvalidException(message="Invalid period entered")
    return crud.create_series(db=db, series=series)

@app.get("/series/{id}", response_model=schemas.Timeseries)
def read_series_by_id(id, db: Session = Depends(get_db)):
    """return timeseries from id given

    Parameters
    ----------
    id: the id of the timeseries

    Returns
    -------
    Timeseries

    """
    id_timeseries = crud.get_series_by_id(db, id=id)
    return id_timeseries

@app.put("/series/{id}", response_model=schemas.Timeseries)
def edit_series_by_id(id, series: schemas.TimeseriesCreate, db: Session = Depends(get_db)):
    """update timeseries from id given

    Parameters
    ----------
    id: the id of the timeseries
    series:
        period: must be a valid ISO 8601 duration
        unit: must be one of: kwh, gbp
        aggregation: must be one of sum, avg, mma

    Returns
    -------
    Timeseries: that has been updated

    """
    try:
        new_duration = iso8601.Duration(series.period)
    # catch value error from duration raise exception
    except ValueError:
        raise exceptions.InvalidException(message="Invalid period entered")
    edited_timeseries = crud.update_series_by_id(db, id=id, series=series)
    return edited_timeseries

@app.delete("/series/{id}")
def remove_series_by_id(id, db: Session = Depends(get_db)):
    """remove timeseries from id given

    Parameters
    ----------
    id: the id of the timeseries

    Returns
    -------
    Confimration message

    """
    del_timeseries = crud.delete_series_by_id(db, id=id)
    return del_timeseries

@app.post("/series/{id}/records", status_code=status.HTTP_204_NO_CONTENT, response_class=Response)
def create_series_records(id, record_set: schemas.TimeseriesRecordSet, db: Session = Depends(get_db)):
    """create records for a timeseries from id given

    Parameters
    ----------
    id: the id of the timeseries
    record_set: the set of records to be added
        Each record contains:
            t: the timestamp - nust be a datetime in ISO 8601 (utc) format
            v: the value for that timestamp

    Returns
    -------
    Nothing

    """
    create_timeseries_record = crud.create_series_records(db=db, record_set=record_set, timeseries_id=id)
    return create_timeseries_record

@app.get("/series/{id}/records")
@app.get("/series/{id}/records/{start}/{end}/{interval}")
def read_series_records_slice(id, start: Optional[str] = "-", end: Optional[str] = "-", interval: Optional[str] = "-", tz: Optional[str] = 'UTC', aggregation: Optional[str] = None, db: Session = Depends(get_db)):
    """return records for a timeseries from id given, for a specified period of
    time at specfied intervals.

    Parameters
    ----------
    id: the id of the timeseries
    start: a datetime in the ISO 8601 format, the start date of the slice inclusive
    end: a datetime in the ISO 8601 format, the end date of the slice exclusive
    interval: the duration between records
    tz: the timezone of the start and end date, an optional query parameter
    needed if the start and end dates are not in utc. The result returned will
    be in this timezone specified (utc if not specifed)
    aggregation: an optional query parameter to specify how the record values
    should be aggregated. If not present the records will be aggregated
    according to the method specified in the associated timeseries. Can be one
    of: sum, mma, avg.

    Returns
    -------
    TimeseriesRecordSet:
        n: the timeseries id for the record
        r: the records for the timeseries
            t: the timestamp
            v: the value

    """
    record_object = prepare_slice(id, start, end, interval, tz, aggregation, db)
    return record_object

@app.websocket("/series/stream")
async def websocket_endpoint(
    websocket: WebSocket,
    db: Session = Depends(get_db)
):
    """return records for a timeseries from id given, for a specified period of
    time at specfied intervals using a websocket.

    Parameters
    ----------
    These parameters are provided as a json object to the websocket

    id: the id of the timeseries
    start: a datetime in the ISO 8601 format, the start date of the slice inclusive
    end: a datetime in the ISO 8601 format, the end date of the slice exclusive
    interval: the duration between records
    tz: the timezone of the start and end date, an optional query parameter
    needed if the start and end dates are not in utc. The result returned will
    be in this timezone specified (utc if not specifed)
    aggregation: an optional query parameter to specify how the record values
    should be aggregated. If not present the records will be aggregated
    according to the method specified in the associated timeseries. Can be one
    of: sum, mma, avg.

    Returns
    -------
    TimeseriesRecordSet:
        n: the timeseries id for the record
        r: the records for the timeseries
            t: the timestamp
            v: the value

    """
    await websocket.accept()
    params = await websocket.receive_json()
    id = params["id"]
    start = params["start"] if "start" in params.keys() else "-"
    end = params["end"] if "end" in params.keys() else "-"
    interval = params["interval"] if "interval" in params.keys() else "-"
    tz = params["tz"] if "tz" in params.keys() else 'UTC'
    aggregation = params["aggregation"] if "start" in params.keys() else None
    # fetch data slice
    # need to agree on consistent "extra" information to return with the timeseries Id and records arrays.
    slice = prepare_slice(id, start, end, interval, tz, aggregation, db)
    json_record = slice.json()
    # send it back
    await websocket.send_text(json_record)
    await websocket.close()

def prepare_slice(id, start, end, interval, tz, aggregation, db):
    # fetch data slice
    try:
        new_duration = iso8601.Duration(interval) if interval != "-" else None
    # catch value error from duration raise exception
    except ValueError:
        raise exceptions.InvalidException(message="Invalid interval entered")
    timeseries = crud.get_series_by_id(db, id=id)

    # check timezone valid
    tz_valid = tz in pytz.all_timezones
    if tz_valid is False:
        raise exceptions.InvalidException(message="Invalid timezone entered")

    # TODO: validate aggregation first
    aggregation_function = aggregation if aggregation is not None else timeseries.aggregation
    period = timeseries.period

    # valid date strings parsed to a datetime, "-" case to None
    # valid intervals can be used for startdatetime

    if start == "-":
        start_parsed = None
        utc_start = None
    else:
        try:
            start_parsed = datetime.datetime.fromisoformat(start)
            utc_start = downsample.convert_local_datetime_to_utc(start_parsed, tz)
        except ValueError:
            try:
                start_parsed = iso8601.Duration(start)
            except:
                raise exceptions.InvalidException(message="Invalid start date entered")
    if end == "-":
        end_parsed = None
        utc_end = None
    else:
        try:
            end_parsed = datetime.datetime.fromisoformat(end)
            utc_end = downsample.convert_local_datetime_to_utc(end_parsed, tz)
        except ValueError:
            if isinstance(start_parsed, iso8601.Duration):
                raise exceptions.InvalidException(message="Both start and end date can not be a duration")
            else:
                try:
                    end_parsed = iso8601.Duration(end)
                except:
                    raise exceptions.InvalidException(message="Invalid end date entered")

    # Compare start and end datetimes
    if type(start_parsed) == datetime.datetime and type(end_parsed) == datetime.datetime:
        if start_parsed >= end_parsed:
            raise exceptions.InvalidException(message="Start date is not before end date")

    # Get start dates when one duration
    if isinstance(start_parsed, iso8601.Duration):
        # if utc_end is None then the get the last record in db call
        if utc_end is None:

            last_record = crud.get_last_series_record(db, timeseries_id=id)
            last_record_dt = downsample.localize_naive_utc_date(last_record.t)
            # end date is not inclusive so should be the last record timestamp
            # plus the timeseries period.
            timeseries_period = iso8601.Duration(period)
            end_dt = timeseries_period.add_duration(last_record_dt)
            utc_start = start_parsed.subtract_duration(end_dt)
        else:
            utc_start = start_parsed.subtract_duration(utc_end)

    if isinstance(end_parsed, iso8601.Duration):
        # if utc_start is None then utc_end should be an interval
        utc_end = end_parsed.add_duration(utc_start) if utc_start is not None else end_parsed

    print(f"utc start {utc_start}")
    print(f"utc end {utc_end}")

    # utc_start = downsample.convert_local_datetime_to_utc(start_parsed, tz) if start_parsed != None else None
    # utc_end = downsample.convert_local_datetime_to_utc(end_parsed, tz) if end_parsed != None else None

    series_records_list = crud.get_series_records_slice(db, timeseries_id=id, start=utc_start, end=utc_end)

    series_records_slice = downsample.downsample(series_records_list, utc_start, utc_end, new_duration, period, aggregation_function, tz)
    records_display_list = [r for r in series_records_slice]

    # logic to update timeseries_id with aggregation method
    timeseries_id_array = [f"{id}|min", f"{id}|max", f"{id}|avg"] if aggregation_function == "mma" else [f"{id}|{aggregation_function}"]

    # need to agree on consistent "extra" information to return with the timeseries Id and records arrays.
    return schemas.TimeserieSliceRecordSet(n=timeseries_id_array, r=records_display_list)

# FastApi example code

@app.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@app.get("/users/", response_model=List[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.post("/users/{user_id}/items/", response_model=schemas.Item)
def create_item_for_user(
    user_id: int, item: schemas.ItemCreate, db: Session = Depends(get_db)
):
    return crud.create_user_item(db=db, item=item, user_id=user_id)


@app.get("/items/", response_model=List[schemas.Item])
def read_items(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    items = crud.get_items(db, skip=skip, limit=limit)
    return items

@app.get("/info")
async def info(settings: config.Settings = Depends(get_settings)):
    return {
        "app_name": settings.app_name,
        "admin_email": settings.admin_email,
        "items_per_user": settings.items_per_user,
    }

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    await websocket.send_json({"msg": "Hello WebSocket"})
    await websocket.close()

# Hello world code

@app.get("/")
async def root():
    return {"message": hello()}

# Start app
def start():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("carbondb.main:app", host="0.0.0.0", port=8000, reload=True)
