""" Database

This module use sqlalchemy to connect to the database and create a database
session
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, registry
from . import config

mapper_registry = registry()
Base = mapper_registry.generate_base()

class Context:
    """
    Context object that lazily creates the DB engine when given settings.
    Lazy loading allows the DB session to only be created when needed and
    in particular makes it possible to delay creating it until its settings
    dependency is resolved. In turn this makes it possible to leverage the
    FastAPI dependency mechanism by making settings an explicit sub-dependency
    of the DB session.
    """
    def __init__(self):
        self.engine = None
    
    def getOrmSession(self, settings):
        if self.engine is None:
            self.engine = create_engine(
                settings.database_url, connect_args={"check_same_thread": False}
            )
            Base.metadata.create_all(bind=self.engine)
        return Session(self.engine)

DbContext = Context()
