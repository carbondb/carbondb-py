""" Models

This module contains the database models
"""

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime, Float
from sqlalchemy.orm import relationship

from .database import Base, DbContext
import datetime

def getOrmSession(settings):
    return DbContext.getOrmSession(settings)

class Timeseries(Base):
    """ Model represents the timeseries information """

    __tablename__ = "timeseries"

    id = Column(String, primary_key=True, unique=True, index=True, autoincrement=False)
    period = Column(String)
    unit = Column(String)
    aggregation = Column(String, default="avg")

class TimeseriesRecord(Base):
    """ Model represents the a single record for a timeseries """

    __tablename__ = "timeseries_records"

    timeseries_id = Column(String, ForeignKey("timeseries.id"), primary_key=True)
    t = Column(DateTime(timezone=True), default=datetime.datetime, primary_key=True)
    v = Column(Float)

# FastApi examples

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    items = relationship("Item", back_populates="owner")


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="items")
