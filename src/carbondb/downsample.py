"""Downsampler

This module contains functions to take a set of TimeSeriesRecords (at a given
sampling rate) and aggregate the data at a lower sampling rate.

downsample is the overarching function that takes the timeseries records,
information on the start and end date (including timezone if required) of a
slice, the sampling rate and aggregation method and returns the new slice of the
timeseries records at the new sampling rate.

The remaining functions and the Bucket class are used to implement the different
stages of downsample:

    * generate_record_list - yields record from record list
    * convert_local_datetime_to_utc - takes a datetime at a specified timezone
    and returns the utc datetime
    * localize_naive_utc_date - takes a timezone naive datetime and localizes to
     utc timezone
    * convert_utc_datetime_to_local - takes a utc datetime and returns a
    datetime for the given timezone.
    * group_to_buckets - yields a set of Buckets from a given record list for
    each interval for the period between the given start and end dates.
    * aggregate_buckets - takes a set of buckets and aggregates the value set
    for each bucket according to the given aggregation method.

"""
import datetime
from . import exceptions, iso8601
from pytz import timezone
import pytz
from .models import TimeseriesRecord
from .schemas import TimeseriesRecordBase, TimeseriesSliceRecordBase
from typing import Iterator, List, Iterable

def generate_record_list(series_record_list: Iterable[TimeseriesRecord])->Iterator[TimeseriesRecord]:
    """Yields a TimeseriesRecord in a list

    Parameters
    ----------
    series_record_list : Iterable[TimeseriesRecord]
        The list of TimeSeriesRecords

    Returns
    -------
    Iterator[TimeseriesRecord]
        generator of the given TimeseriesRecords
    """
    for rec in iter(series_record_list):
        yield rec

class Bucket:
    """A class used to represent a sampling period with the starting timestamp
    and the associated values for the time period.

    ...

    Attributes
    ----------
    t: datetime
        a datime in ISO 8601 format
    v: List[float]
        a set of values for the sampling period beginning at t

    """
    t: datetime.datetime
    v: List[float]
    def __init__(self, t, v):
        self.t = t
        self.v = v
    def __eq__(self, other):
        return self.v == other.v and self.t == other.t

def convert_local_datetime_to_utc(date, tz):
    """takes a datetime at a specified timezone and returns the utc datetime

    Parameters
    ----------
    date : datetime
        A datetime to be converted to utc
    tz: str
        The local timezone (pytz timezone definition) of the date given.

    Returns
    -------
    datetime
        datetime utc timezone
    """
    query_timezone = timezone(tz)
    utc_timezone = pytz.utc
    local_date = query_timezone.localize(date)
    # get utc time
    utc_date = local_date.astimezone(pytz.utc)
    return utc_date

def localize_naive_utc_date(date):
    """takes a timezone naive datetime and localizes to
     utc timezone

    Parameters
    ----------
    date : datetime
        A timezone naive datetime to be localised to utc

    Returns
    -------
    datetime
        datetime utc timezone
    """
    utc_timezone = pytz.utc
    tz_aware_date = utc_timezone.localize(date)
    return tz_aware_date

def convert_utc_datetime_to_local(utc_datetime, tz):
    """takes a utc datetime and returns a datetime for the given timezone.

    Parameters
    ----------
    utc_datetime : datetime
        A datetime which is timezone aware and utc
    tz: str
        The local timezone (pytz timezone definition) the date should be
        returned in.

    Returns
    -------
    datetime
        datetime in the given local timezone

    """
    query_timezone = timezone(tz)
    # get local time
    local_date = utc_datetime.astimezone(query_timezone)
    return local_date


def group_to_buckets(series_record_list: Iterator[TimeseriesRecord], start, end, interval):
    """Yields a set of Buckets from a given record list for each interval for
    the period between the given start and end dates.

    Parameters
    ----------
    series_record_list : Iterator[TimeseriesRecord]
        The list of TimeSeriesRecords
    start: datetime
        The start date - in ISO 8601 format and timezone aware
    end: datetime
        The end date - in ISO 8601 format and timezone aware
    interval: Duration
        The sampling rate required
    Returns
    -------
    Iterator[Bucket]
        a generator containing bucket instances for each of the new downsampled
        intervals and the associated values for that interval.
    """
    # Need to get start and end dates from the buckets if they are set to None
    if start is not None:
        start_t = start
        end_t = interval.add_duration(start_t)
        # Get end date if end is a duration
        if isinstance(end, iso8601.Duration):
            end = end.add_duration(start_t)
    record = None
    try:
        record = next(series_record_list)
        if start is None:
            start_t = localize_naive_utc_date(record.t)
            end_t = interval.add_duration(start_t)
            # Get end date if end is a duration
            if isinstance(end, iso8601.Duration):
                end = end.add_duration(start_t)
        while record is not None and localize_naive_utc_date(record.t) < start_t:
            try:
                record = next(series_record_list)
            except StopIteration:
                record = None
    except StopIteration:
        pass
    while (end is not None and start_t < end) or (end is None and record is not None):
        bucket = []
        while record is not None and localize_naive_utc_date(record.t) < end_t:
            if record.v is not None:
                bucket.append(record.v)
            try:
                record = next(series_record_list)
            except StopIteration:
                record = None
        yield Bucket(t=start_t, v=bucket)
        start_t = end_t
        end_t = interval.add_duration(start_t)

def aggregate_buckets(buckets, aggregation, tz):
    """takes a set of buckets and aggregates the value set for each bucket
    according to the given aggregation method.

    Parameters
    ----------
    buckets : Iterator[Buckets]
        The list of downsampled Buckets
    aggregation: str
        The aggregation method for the interval values. Can be "sum", "mma" or
        "avg" (avg is the default method)
    tz: timezone
        The local timezone of the start and end dates for the buckets given

    Returns
    -------
    Iterator[TimeseriesSliceRecordBase]
        a generator containing TimeseriesSliceRecordBase instances for each of
        the new downsampled intervals and the aggregated values for that
        interval.
    """
    for r in buckets:
        values = r.v
        if len(values) == 0:
            yield TimeseriesSliceRecordBase(t=convert_utc_datetime_to_local(r.t, tz), v=[None])
        elif aggregation == "sum":
            yield TimeseriesSliceRecordBase(t=convert_utc_datetime_to_local(r.t, tz), v=[sum(values)])
        elif aggregation == "mma":
            yield TimeseriesSliceRecordBase(t=convert_utc_datetime_to_local(r.t, tz), v=[min(values), max(values), sum(values)/len(values)])
        else:
            print("in else logic")
            yield TimeseriesSliceRecordBase(t=convert_utc_datetime_to_local(r.t, tz), v=[sum(values)/len(values)])

def downsample(series_record_list, start, end, interval, period, aggregation, tz):
    """returns a set of TimeSeriesRecords at the specified sampling rate for the
     period between the start and end dates given.

    Parameters
    ----------
    series_record_list : Iterable[TimeSeriesRecords]
        The list of TimeSeriesRecords
    start: datetime
        The start date - in ISO 8601 format and utc timezone
    end: datetime
        The end date - in ISO 8601 format and utc timezone
    interval: Duration
        The sampling rate required
    period: str
        The period specified in the Timeseries - which is the original sampling
        interval
    aggregation: str
        The aggregation method for the interval values. Can be "sum", "mma" or
        "avg" (avg is the default method)
    tz: timezone
        The local timezone of the start and end dates for the buckets given

    Returns
    -------
    Iterator[TimeseriesSliceRecordBase]
        a generator containing TimeseriesSliceRecordBase instances for each of
        the new downsampled intervals and the aggregated values for that
        interval.
    """
    valid_interval = iso8601.Duration(period) if interval == None else interval
    return aggregate_buckets(
        group_to_buckets(
            generate_record_list(series_record_list),
            start, end, valid_interval
        ),
        aggregation, tz
    )
